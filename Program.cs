﻿using System;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {

            int squareSize;

            //Prompt user for the size of the square
            Console.Write("Please enter the square size: ");
            squareSize = Convert.ToInt32(Console.ReadLine());


            //Draw the upper part of the square based on the user input
            for (int i = 0; i < squareSize; i++) Console.Write("*");
            Console.Write("\n");

            //Draw the middle part of the square based on the user input
            for (int i = 0; i < squareSize - 2; i++)
            {
                Console.Write("*");
                for (int j = 0; j < squareSize - 2; j++) Console.Write(" ");
                Console.Write("*");
                Console.Write("\n");

            }

            //Draw the lower part of the square based on the user input
            for (int i = 0; i < squareSize; i++) Console.Write("*");
            Console.Write("\n");
        }
    }
}
